# Projects #

### 01.09.2014 - 31.12.2014 Patty (Nortal AS) ###

* As part of a course Patty was born.
* Idea behind the project is to enable coworkers to praise and acknowledge each others efforts and deeds within and outside their responsibilities.
* My responsibilities (full stack):
    * Software architecture 
    * Software design
    * UI design
    * UX design
    * Programming
* Technology stack:
    * Java
    * Javascript
    * NoSQL
    * Spring MVC
    * Spring Security
    * AngularJS (1.x)
    * MongoDB
    * JUnit
* [Description of the project](https://courses.cs.ut.ee/MTAT.03.138/2014_fall/uploads/Main/Patty%20description.pdf)

### 17.04.2015 - 04.12.2015 Virtual neuroscience toolbox (University of Tartu / Virtual Neuroscience Lab) ###

* As part of by BSc thesis a "Toolbox for creating virtual reality environments for memory research" was developed. It was made in collaboration with 2 psychology students (Madis Vasser (MSc), Taavi Kivisik (MSc)), 1 computer engineering student (Markus Kängsepp (BSc)) and 2 supervisors (Jaan Aru (PhD), Raul Vincete(PhD)).
* The toolbox has random game room generator, level editor and an experiment creator. It will supports four different types of memory experiment methods.
* My responsibilities (full stack):
    * Software architecture 
    * Software design
    * UI design
    * UX design
    * Creating, saving and editing 3D environments
    * Generating random set of rooms (predefined room blocks which were fit together to form game levels)
    * Filling rooms with randomly placed objects (predefined locations and object types chosen from object pool)
    * 4 different quiz types to evaluate subjects recall
    * Oculus support
* Technology stack:
    * Unity
    * C#
    * Oculus SDK
* [Thesis and screenshots of the toolbox](http://dspace.ut.ee/handle/10062/56099)

### 01.04.2015 - 31.05.2015 Street-U ###

* R&D with computer vision. Goal was to recognize faces, license plates and speed signs from panoramic images.
* My responsibilities:
    * Understanding and testing different algorithms, techniques and libraries. 
* Technology stack:
    * Java
    * C++
    * Python
    * OpenCV

### 01.06.2015 - 18.09.2015 Street-U V2 ###

* Street-U V2 is a web-based application that allows its users to observe city environment, perform measurements, collect data and share information.
* My responsibilities (full stack):
    * UI design
    * UX design
    * Front-end business logic
        * 3D word (viewing panoramic images, measurements etc.)
        * Managing collected data (CRUD operations between 3D world and back-end)
        * User management
    * Mobile-ID and ID-card authentication on back-end
    * Front-end and back-end logic for integration with Geoserver (wfs, wms, shp services)
    * R&D Oculus plugin for viewing and moving around in 3D world (websockets)
    * R&D WebVR plugin for viewing and moving around in 3D world (HTML5 experimental APIs)
* Technology stack:
    * Javascript
    * AngularJS 1.x
    * Three.js
    * Grunt
    * Bower
    * CSS
    * Bootstrap
    * OpenLayers 2.x
    * Java
    * Spring
    * JUnit
    * PostgreSQL
    * Liquibase
    * Geoserver
* [Description of the application](http://www.reach-u.com/eyevi.html)

### 18.09.2015 - 27.11.2015 RIKS ###

* Analyzing and visualizing network operators signal strength and cell quality.
* My responsibilities (front-end):
    * UI design
    * R&D code to best visualize network operator data and signal strength
    * Tool to dynamically filter and visualize around 1 mil. data points on map
* Technology stack:
    * Javascript
    * Leaflet

### 18.09.2015 - 27.12.2015 Demograft ###

* Demograft is a platform for collecting and analyzing a massive amount of passive location updates from mobile operator’s networks with no additional load to the radio network.
* My responsibilities (full stack):
    * UI design
    * Front-end business logic
        * Data manager to enable users to add, modify and remove data (for back-end processing)
        * Map legend to explain complex queries and data structures displayed on map
    * Back-end business logic
        * Processing data files
    * Front-end and back-end logic for integration with Geoserver (wfs, legend generation)
* Technology stack:
    * Javascript
    * AngularJS 1.x
    * Gulp
    * CSS
    * Bootstrap
    * Google maps
    * Java
    * Spring
    * RabbitMQ
    * Drake
    * JUnit
    * Geoserver
* [Description of the application](http://www.reach-u.com/demograft.html)

### 30.11.2015 - 31.12.2015 Delfi ###

* Public map application
* My responsibilities (full stack):
    * Front-end business logic
        * Developing extra components, integrations and functionality per client's request
    * Back-end business logic
        * Developing extra components, integrations and functionality per client's request
        * POI updates
* Technology stack:
    * Javascript
    * CSS
    * OpenLayers
    * PHP
    * PostgreSQL
* [Live application](https://kaart.delfi.ee/)

### 30.11.2015 - 31.05.2016 Eyevi (Street-U V2 rewrite + additional functionality) ###

* Eyevi is a web-based application that allows its users to observe city environment, perform measurements, collect data and share information.
* My responsibilities (full stack):
    * UI design
    * UX design
    * Front-end business logic
        * 3D word (viewing point cloud and performing measureviewing panoramic images and point cloud, measurements)
            * Loading, visualizing and viewing point cloud and panoramic images.
            * Measurement tools for point cloud, panoramic images and map service (ortho images)
        * Managing collected data (CRUD operations between 3D world and back-end)
        * User management
    * Back-end business logic
        * CRUD operations for front-end
    * Integrations with MapServer, MapProxy
    * MapServer and MapProxy performance tuning
* Technology stack:
    * Javascript
    * Knockout
    * Three.js
    * Gulp
    * CSS (Sass)
    * Leaflet
    * PHP
    * PostgreSQL
    * MapServer
    * MapProxy
* [Description of the application](http://www.reach-u.com/eyevi.html)

### 27.05.2016 - 16.07.2017 Teeregister (for Estonian Road Administration) ###

* Application to administer countries infrastructure
* My responsibilities (back-end):
    * Back-end business logic
        * Infrastructure objects management (similar to version control system)
        * Statement services (complex algorithms by which budgets are set)
        * Import/export from and to registry (excel, csv, pdf, charts)
        * Component to process shp, dwg, dxf files to PostGIS geometries
        * Search services
        * 7 integrations with different applications and government agencies
        * CRUD operations for front-end
        * User management
    * Mobile-ID, ID-card authentication
    * ActiveDirectory authentication
    * Integrations with MapServer
    * Performance tuning (Java and SQL optimizations)
* Technology stack:
    * Java
    * Spring
    * Jooq
    * PostgreSQL
    * MapServer
* [Live application](https://teeregister.mnt.ee/reet)

### 16.07.2017 - 16.10.2017 (left the company, project ongoing) Baltic Geodata Marketplace (Reach-U Ltd, Karšu izdevniecība Jāņa sēta SIA) ###

* Sales system of unified geospatial products and services of two largest geospatial sector companies in the Baltic region
* My responsibilities (distributed back-end system):
    * API architecture
    * Geodata processing and generalization
    * Geocoding
    * Reverse geocoding
    * Routing
    * API key management and token authentication
    * Other business logic
* Technology stack:
    * Java
    * Dropwizard
    * Testcontainers
    * Liquibase
    * Jooq
    * PostgreSQL
    * Docker
    * MapServer
    * MapProxy
    * OpenStreetMap
    * OSRM
    
### 01.11.2017 - ongoing Zooplus AG ###

* Developing the product of the company a e-commerce shop. 39 active shops in Europe, around 200k orders every month and 1.1B euros of sales yearly.
* My responsibilities (distributed back-end system):
    * API architecture
    * API implementation
    * API deployment
    * API monitoring
    * KPI measurements and improvements
    * Other business logic
* Technology stack:
    * Monolith side
      * Java
      * Spring
      * Hibernate
      * Oracle
      * Tomcat
      * Docker
      * Apache Mesos
      * ELK stack
      * Grafana
    * Microservice side
      * Contentful (CMS)
      * Node.js
      * Typescript
      * AWS DynamoDB
      * AWS S3
      * AWS Lambda
      * AWS CloudWatch
      * AWS API Gateway
      * AWS CloudFront
      * AWS Route53
      